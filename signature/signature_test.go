package signature

import (
	"testing"
	"time"
)

const (
	appID      = 123456

	privateKey = `-----BEGIN PRIVATE KEY-----
MIGEAgEAMBAGByqGSM49AgEGBSuBBAAKBG0wawIBAQQgbYlnwzxMuq7jn49dMPNl
liqIJfjX0kOHFECkNGif6aGhRANCAARZNADLVPmrty/bWYaxFGZFLgKHO6xlyR+S
ai+MJExqqwfeqQUuUAxcmiAchhZF4s65sebZndxEXxnY7RBCQ8na
-----END PRIVATE KEY-----
`

	publicKey = `-----BEGIN PUBLIC KEY-----
MFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEWTQAy1T5q7cv21mGsRRmRS4ChzusZckf
kmovjCRMaqsH3qkFLlAMXJogHIYWReLOubHm2Z3cRF8Z2O0QQkPJ2g==
-----END PUBLIC KEY-----
`
)

func TestSignature_Generate_Verify(t *testing.T) {
	s := New(appID)
	sign, err := s.Generate("1", privateKey)
	if err != nil {
		t.Fatal(err)
	}

	b, err := s.Verify(sign, publicKey)
	if err != nil {
		t.Fatal(err)
	}

	if !b {
		t.Error("Verify fail")
	}
}

func TestSignature_Verify_Expire(t *testing.T) {
	s := New(appID).WithExpire(0)
	sign, err := s.Generate("2", privateKey)
	if err != nil {
		t.Fatal(err)
	}

	time.Sleep(time.Second)

	b, _ := s.Verify(sign, publicKey)
	if b {
		t.Error("Verify Expire fail")
	}
}
