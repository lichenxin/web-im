#！/bin/bash

yarn build

./bin/go-bindata -o=data/data.go -pkg=data dist/...
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags '-w -s' -o bin/web-im
CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -ldflags '-w -s -H windowsgui' -o bin/web-im.exe

echo "OK"