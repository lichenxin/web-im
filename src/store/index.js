import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    title: 'web-im',
    login: {
        user: {},
    },
  },

  mutations: {
    commitLogin(state, value) {
        state.login.user = value
    },
  },

  actions: {

  }
});

const modules = require.context('./modules', true, /\.js$/);

modules.keys().forEach(modules => {
    store.registerModule(modules.replace(/(^\.\/)|(\.js$)/g, ''), modules(modules).default)
});

export default store