import Ajax from "axios"
import QS from 'qs'

let api = Ajax.create({
    baseURL: '/',
    timeout: 100000,
});

// 请求拦截器
api.interceptors.request.use(
    request => {
        return request;
    },
    error => {
        return Promise.reject(error);
    }
);

// 返回拦截器
api.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        return Promise.reject(error);
    }
);

// http request
export default {
    install(Vue) {
        Vue.prototype.$http = this;
    },

    get(uri, params) {
        return this.request({method: 'GET', url: uri, params});
    },

    post(uri, params) {
        return this.request({method: 'POST', url: uri, data: QS.stringify(params)});
    },

    put(uri, params) {
        return this.request({method: 'PUT', url: uri, data: QS.stringify(params)});
    },

    delete(uri, params) {
        return this.request({method: 'DELETE', url: uri, params});
    },

    patch(uri, params) {
        return this.request({method: 'PATCH', url: uri, data: QS.stringify(params)});
    },

    request(params) {
        return api.request(params);
    },
};