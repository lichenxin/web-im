import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import http from './libs/http'
import VueAnt from 'ant-design-vue'

// css
import 'ant-design-vue/dist/antd.css'

Vue.config.productionTip = false;

Vue.use(VueAnt);
Vue.use(http);

new Vue({
    router,
    store,
    render: h => h(App),
    created() {
        console.log('main created');
    },
    mounted() {
        console.log('main mounted');
    },
}).$mount('#app');