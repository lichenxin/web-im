// https://cli.vuejs.org/zh/config/
const path = require('path')

function resolve (dir) {
    return path.join(__dirname, dir)
}

module.exports = {
    devServer: {
        host: '0.0.0.0',
        port: 9600,
        proxy: 'http://127.0.0.1:9008'
    },
    productionSourceMap: false,
    configureWebpack: {
        resolve: {
            extensions: ['.js', '.css', '.vue'],
            alias: {
                '@': resolve('src'),
            }
        },
    }
}