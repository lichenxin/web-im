### 腾讯云通信web端测试工具

```
yarn install

yarn build

./bin/go-bindata -o=data/data.go -pkg=data dist/...
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags '-w -s' -o bin/web-im
CGO_ENABLED=0 GOOS=windows GOARCH=amd64 go build -ldflags '-w -s -H windowsgui' -o bin/web-im.exe

echo "OK"
```

### window 生成带ico图标的exe
```
1. 安装 MinGW
下载地址：http://www.mingw-w64.org/doku.php/download

2.在 main.go 的同级目录下 新建文件 main.rc ，文件内容如下
IDI_ICON1 ICON "ICON.ico"

3.在 main.rc 的同级目录下 放置一个 ICON.ico 的图标文件
在当前目录输入CMD命令： windres -o main.syso main.rc

4.编译golang
go build -ldflags="-H windowsgui"
```
